#!/usr/bin/env node

var amqp = require('amqplib/callback_api');
//
/*var express = require('express');
var app= express();
var server=require('http').Server(app);
var serialport=require('serialport');

var myPort=new serialport("COM4",{
    baudRate: 38400,
    parser: new serialport.parsers.Readline('\r\n')// parsers.Readline('\n'),        
});
myPort.on('open',onOpen);   
myPort.on('data',onData); 

function onData(dato) {
    //console.info(dato.toString());
}
function onOpen() {
    console.log('conectado');
}
server.listen(8000,function () {
    console.log('El servidor arranco');
})*/
amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }
        var exchange = 'topic_logs';
        var args = process.argv.slice(2);       
        var key = (args.length > 0) ? args[0] : 'temp';
        var msg = args.slice(1).join(' ') || "HELLO";

        

        channel.assertExchange(exchange, 'topic', {
            durable: false
        });
        channel.publish(exchange, key, Buffer.from(msg));
        console.log(" [x] Sent %s: '%s'", key, msg);
    });
    setTimeout(function() {
        connection.close();
        process.exit(0);
    }, 500);
});